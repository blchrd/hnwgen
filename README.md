# HNW Generator

## Foreword

This project was rewritten in Rust.

I'm not sure I'll work on this version in the future.

[The new repository.](https://framagit.org/blchrd/rust-hnwgen)

## Description

Generate a [Harsh Noise Wall](https://en.wikipedia.org/wiki/Harsh_noise_wall)
album and its artwork cover.

In short, here what is happening when generating a track :

1. Generate noise source
2. Apply some EQ and / or pitch change with FFmpeg
3. Do 1 and 2 multiple time (one for each layer of noise)
4. Mix all the layers

You can of course release the output like one of your album, if you do so, please include a link to this page in the album credits.

## Releases

* The [Windows release](https://anadrark.com/hnwgen/hnwgen-0.1-win.zip) contains JRE 15 and two executable, hnwgen-gui.exe and hnwgen.exe (this one is a lot heavier than the other)
* The [other release](https://anadrark.com/hnwgen/hnwgen-0.1.zip) contains two jar files, hnwgen-gui.jar and hnwgen.jar, it works on Linux and on Windows.

Both zip archive contains this README on markdown format

## Getting started

### Dependencies

This project requires the following dependencies:

* FFmpeg
* Java JRE 15

#### Windows Environment

**If you use the Windows release, you can skip this section.**

To run this project on a Windows environment, make sure both FFmpeg and Java JRE 15 are included in your system's PATH.

1. Download and install FFmpeg from the official website: [FFmpeg](https://ffmpeg.org/download.html)
2. Download and install Java JRE 15 from the official Oracle
   website: [Java JRE 15](https://www.oracle.com/java/technologies/javase/jdk15-archive-downloads.html)

After installing both dependencies, here's how you can add them to the PATH:

1. Open the Start menu and search for "Environment Variables" and select "Edit the system environment variables".
2. In the System Properties window, click on the "Environment Variables" button.
3. In the "System Variables" section, select the "Path" variable and click on the "Edit" button.
4. In the "Edit Environment Variable" window, click on the "New" button.
5. Enter the path to the FFmpeg installation directory (e.g., `C:\path\to\ffmpeg`) and click "OK".
6. Repeat step 5 for the Java JRE 15 installation directory (e.g., `C:\path\to\java`).
7. Click "OK" to close all the windows.

#### Linux Environment

If you are using a Linux environment, you can install the dependencies directly without modifying the PATH.

Open the terminal and execute the following command:

`sudo apt-get install ffmpeg openjdk-15-jre`

### Usage

Download the zip in /release/ and extract it on your computer.

The release zip contains 2 jar files or two exe file + dependencies if you download the Windows release:

`hnwgen-gui.jar|exe` is the GUI version, it's really basics right now, but it's enough for generate an album.

`hnwgen.jar|exe` is the CLI version, you can use it in a terminal with the command line below (and all the parameters we define in the
next section)

To launch the jar files, use these commands in a terminal:

* `java -jar hnwgen-gui.jar`
* `java -jar hnwgen.jar` launch the generation with default parameters
* `java -jar hnwgen.jar -h` display the help

The album generated will be in a "generation" folder, in your home directory or in the .jar directory

#### Command line parameters

`-h --help` display the help

`--gui` display gui

`--length=<int>` total duration of album in minute

`--nbTracks=<int>` number of tracks in album

`--nbLayers=<int>` number of layers for each track

`--pitch` enable pitch changer in layer generation

`--variation` enable wall variation in layer generation

`--taintCover` enable the tainting of generated cover

`--onlycover` only generate the cover

`--onlytributecover=<file>` only generate the tribute cover, based on the file

`--tribute=<directory>` directory of original wav file for tribute album, the track will have 10 minutes length

`--tributetitle=<string>` title of tribute, for cover generation

`--withcover` enable the generation of cover for tribute album. Need a file named "cover.jpg" in tribute directory

### Configuration file

Beside the executable file, you can put a file `hnw.conf` with the data below.

Configuration key :

| Key                        | Description                                                                                        |
|----------------------------|----------------------------------------------------------------------------------------------------|
| project_name               | Name of the project. If it's empty, the name will be "unknown"                                     |
| album_title_template       | Template for the title of the album. If it's empty, the title will be "untitled"                   |
| cover_album_title_template | Template for the title on the cover. If it's empty, this will be %%PROJECTNAME%% - %%ALBUMTITLE%%" |
| track_title_template       | Template for the track title, if it's empty, the title will be "untitled"                          |

Here is the template possible value :

| Template        | Replacing string          |
|-----------------|---------------------------|
| %%PROJECTNAME%% | Name of the project       |
| %%ALBUMTITLE%%  | Title of the album        |
| %%RANDHEX%%     | Random hexadecimal number |

For example, here the configuration file I used for testing:

    project_name=getStatic();
    album_title_template=data_%%RANDHEX%%
    cover_album_title_template=getStatic(%%ALBUMTITLE%%);
    track_title_template=segment_0x%%RANDHEX%%

## Authors and acknowledgment

Special thanks to JLIAT for authorizing me to use its [noise generator](http://www.jliat.com/HNW/index.html).

## Licence

This work is licenced under GPLv3 licence.

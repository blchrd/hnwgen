package io.hnwgen.hnw;

import io.hnwgen.ffmpeg.FFMPEG;

public class HnwEQ {
    public enum Freq {LOW, MID, HIGH}

    int frequency;
    int gain;
    int width;
    HnwRandom random;

    public HnwEQ() {
        random = new HnwRandom();
        this.width = random.getRandomBetween(1, 3);
        this.width = this.width * 100;
    }

    public void initEQ(Freq freq) {
        switch (freq) {
            case LOW:
                this.setRandomFrequency(250, 500);
                this.setRandomGain(5, 11);
                this.setRandomWidth(200, 400);
                break;
            case MID:
                this.setRandomFrequency(700, 3500);
                this.setRandomGain(1, 10, true);
                break;
            case HIGH:
                this.setRandomFrequency(3500, 7000);
                this.setRandomGain(1, 10, true);
                this.setRandomWidth(100, 300);
                break;
        }
    }

    public int getFrequency() {
        return this.frequency;
    }

    public int getGain() {
        return this.gain;
    }

    public void setRandomFrequency(int min, int max) {
        int tempMin = min / 100;
        int tempMax = max / 100;

        this.frequency = this.random.getRandomBetween(tempMin, tempMax);
        this.frequency = this.frequency * 100;
    }

    public void setRandomWidth(int min, int max) {
        int tempMin = min / 100;
        int tempMax = max / 100;

        this.width = this.random.getRandomBetween(tempMin, tempMax);
        this.width = this.width * 100;
    }

    public void setRandomGain(int min, int max) {
        this.setRandomGain(min, max, false);
    }

    public void setRandomGain(int min, int max, boolean withNegative) {
        this.gain = this.random.getRandomBetween(min, max, withNegative);
    }

    public void apply(String in, String out) {
        FFMPEG.applyEq(in, out, this.frequency, this.width, this.gain);
    }

    public void apply(String in, String out, Freq freq) {
        this.initEQ(freq);
        this.apply(in, out);
    }
}

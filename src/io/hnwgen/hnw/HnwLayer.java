package io.hnwgen.hnw;

import io.hnwgen.ffmpeg.FFMPEG;

public class HnwLayer {
    String outFile;
    int length;
    String path;
    int loudness;
    HnwRandom random;
    String eqDetails;
    boolean pitchChance;
    double pitchRate;
    boolean variationChance;

    public HnwLayer(String outFile, String path, int length, int loudness, boolean pitchChance, boolean variationChance, HnwRandom random) {
        this.random = random;
        this.outFile = outFile;
        this.loudness = loudness;
        this.length = length;
        this.path = path;
        this.pitchChance = pitchChance;
        this.variationChance = variationChance;
    }

    public void generate() {
        //File path variables
        String outputFileAU = this.path + "/" + this.outFile + ".AU";
        String outputFileWAV = this.path + "/" + this.outFile;
        String vanillaTempFile = this.path + "/temp.wav";
        String tempFileEQ = this.path + "/final.wav";
        String tempFileResampled = this.path + "/final2.wav";

        int trackHarsh = random.nextInt(2500) + 80;
        boolean variation = (this.variationChance && this.random.randomBool());
        System.out.println("Harshness : " + trackHarsh);
        System.out.println("Variation : " + variation);

        //Call the HNW Magick maker
        HnwFile hnwFile = new HnwFile(trackHarsh, this.length, this.loudness, variation);
        hnwFile.generateAudio(outputFileAU);

        //Convert .AU --> .wav
        FFMPEG.convertFile(outputFileAU, vanillaTempFile);

        //apply equalizer
        this.applyEQ(vanillaTempFile, tempFileEQ);

        //Resample the file 44.1kHz
        FFMPEG.resample(tempFileEQ, tempFileResampled);

        //Calculate pitch chance and apply pitch if necessary
        this.pitchRate = this.pitchChance && random.getRandomBetween(0, 2) == 1
                ? 1.0
                : (double) random.getRandomBetween(2, 15) / 10;
        if (this.pitchRate != 1.0) {
            String tempFilePitched = this.path + "/final3.wav";
            System.out.println("Pitch : " + this.pitchRate);
            FFMPEG.applyPitch(tempFileResampled, tempFilePitched, this.pitchRate);
            tempFileResampled = tempFilePitched;
        }

        //Finalize the layer
        FFMPEG.finalize(tempFileResampled, outputFileWAV);
    }

    private void applyEQ(String inFile, String outFile) {
        //File path variables
        String tempFile1 = this.path + "/temp2.wav";
        String tempFile2 = this.path + "/temp3.wav";

        //low frequency
        HnwEQ equalizer = new HnwEQ();
        equalizer.apply(inFile, tempFile1, HnwEQ.Freq.LOW);
        this.eqDetails = equalizer.getFrequency() + "Hz " + equalizer.getGain() + "db";
        //medium frequency
        if (this.random.randomBool()) {
            equalizer.apply(tempFile1, tempFile2, HnwEQ.Freq.MID);
            this.eqDetails += " / " + equalizer.getFrequency() + "Hz " + equalizer.getGain() + "db";
            //high frequency
            equalizer.apply(tempFile2, outFile, HnwEQ.Freq.HIGH);
        } else {
            equalizer.apply(tempFile1, outFile, HnwEQ.Freq.MID);
        }
        this.eqDetails += " / " + equalizer.getFrequency() + "Hz " + equalizer.getGain() + "db";
    }

    public String getEqDetails() {
        return this.eqDetails + " / Pitchrate : " + this.pitchRate;
    }
}

package io.hnwgen.hnw;

import io.hnwgen.ffmpeg.FFMPEG;

public class HnwTrack {
    HnwRandom random;
    HnwLayer[] layers;
    String path;
    String title;
    int length;
    int loudness;
    boolean pitchChance;
    boolean variationChance;

    public HnwTrack(String title, int length, String path, int loudness, int nbLayers, boolean pitchChance, boolean variationChance, HnwRandom random) {
        this.title = title;
        this.length = length;
        this.path = path;
        this.layers = new HnwLayer[nbLayers];
        this.random = random;
        this.loudness = loudness;
        this.pitchChance = pitchChance;
        this.variationChance = variationChance;
    }

    public void generateLayers() {
        System.out.println("Generate track " + this.title);
        for (int i = 0; i < this.layers.length; i++) {
            System.out.println("Generate layer " + (i + 1) + " out of " + this.layers.length);
            this.layers[i] = new HnwLayer(this.title + "-" + i + ".wav", this.path, this.length, this.loudness, this.pitchChance, this.variationChance, this.random);
            this.layers[i].generate();
        }
    }

    public void mixLayers() {
        String[] layersPath = new String[this.layers.length];
        int index = 0;
        for (HnwLayer layer : this.layers) {
            layersPath[index] = layer.path + "/" + layer.outFile;
            index++;
        }

        FFMPEG.mixLayers(layersPath, this.path + "/" + this.title + ".wav");
    }

    public String getFilePath() {
        return this.path + "/" + this.title + ".wav";
    }

    public String getFileName() {
        return this.title + ".wav";
    }

    public String getTrackDetails() {
        String details = this.title;
        int numLayer = 1;
        for (HnwLayer layer : this.layers) {
            details += "\r\n  Layer " + numLayer + " : EQ " + layer.getEqDetails();
            numLayer++;
        }
        return details;
    }
}

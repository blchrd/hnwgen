package io.hnwgen.hnw;

import java.util.Random;

public class HnwRandom {
    Random random;

    public HnwRandom() {
        this.random = new Random();
    }

    public int getRandomBetween(int min, int max) {
        return getRandomBetween(min, max, false);
    }

    public int getRandomBetween(int min, int max, boolean withNegative) {
        int randInt = this.random.nextInt(max + 1 - min) + min;
        if (withNegative && this.randomBool()) {
            randInt = randInt * -1;
        }

        return randInt;
    }

    public boolean randomBool() {
        return (this.random.nextInt(2) == 1);
    }

    public int nextInt() {
        return this.random.nextInt();
    }

    public int nextInt(int bound) {
        return this.random.nextInt(bound);
    }
}

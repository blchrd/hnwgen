package io.hnwgen.hnw;

import java.io.FileOutputStream;
import java.util.Random;

//Class from http://www.jliat.com/HNW/index.html

public class HnwFile {
    int harshness;
    int noiselen;  // length of piece
    int loudness;
    Random generator;
    boolean variation;

    public HnwFile(int harsh, int len, int loud, boolean variation) {//constructor
        this.harshness = harsh;
        this.noiselen = len;
        this.loudness = loud;
        this.generator = new Random();
        this.variation = variation;
        if (this.variation) System.out.println("With variation");
    }

    public void generateAudio(String outfile) {
        try {
            FileOutputStream out;
            out = new FileOutputStream(outfile);
            // 16000 16-bit mono signed big endian
            // size of number of samples to smooth
            // lager = more coarse sounds
            out.write(46); // .
            out.write(115); // S
            out.write(110); // N
            out.write(100); // D
            // Header size
            out.write(0); // 0
            out.write(0); // 0
            out.write(0); // 0
            out.write(24); // 24
            // Data size - default ffff
            out.write(255); // ff
            out.write(255); // ff
            out.write(255); // ff
            out.write(255); // ff
            // sample type 3 = pcm
            out.write(0); // 0
            out.write(0); // 0
            out.write(0); //
            out.write(3); //
            // sample rate 16000
            out.write(0); // 0
            out.write(0); // 0
            out.write(62); // 16000
            out.write(128); //
            // 2 channels
            out.write(0); // 0
            out.write(0); // 0
            out.write(0); //
            out.write(2); //
            // harshness the higher this value the more rough / harsh
            int harshnessTemp = this.harshness;
            int harshnessRand = this.generator.nextInt(3) + 1;

            double[] ss = new double[harshnessTemp];
            double sa;
            short s; // signed two byte integer  for PCM data

            boolean variation = false;

            for (int z = -1; z < this.noiselen; z++) { // make this big for long pieces or
                if (this.variation) {
                    if (this.generator.nextInt(150) == 0) {
                        if (!variation) {
                            System.out.println("Variation starting at "+z);
                            variation = true;

                            harshnessTemp = this.generator.nextInt(2500) + 80;
                        } else {
                            System.out.println("Variation ending at "+z);
                            variation = false;

                            harshnessTemp = this.harshness;
                        }
                        ss = new double[harshnessTemp];
                    }
                }

                // if noiselen was 0 from commandline run forever!
                // note only play half buffer to stop gaps???
                if (this.noiselen < 1) z = -2;
                //----------------------- loop ------------------------

                for (int cnt = 0; cnt < 1000; cnt = cnt + 2) {
                    int r = this.generator.nextInt();

                    s = (short) r;
                    // store new sample - roll out others
                    // end of x for
                    if (harshnessTemp - 1 >= 0) System.arraycopy(ss, 1, ss, 0, harshnessTemp - 1);
                    ss[harshnessTemp - 1] = s;
                    // average  the last harshness number of samples
                    // remove the higher pitches
                    sa = 0;
                    for (int x = 0; x < harshnessTemp; x++) {
                        sa = sa + ss[x];
                    }
                    int harshnessDiv = harshnessTemp / (this.generator.nextInt(harshnessRand) + 1);
                    if (harshnessDiv == 0) {
                        harshnessDiv = 1;
                    }
                    sa = sa / (harshnessDiv);
                    s = (short) sa;
                    //============ Process  for new sounds ========

                    //	   loudness====  !!!!
                    // basically no wimpy numbers - only fat ones
                    if (s > 0 & s < this.loudness) {
                        s = (short) (s + this.loudness);
                    }
                    if (s < 0 & s > -this.loudness) {
                        s = (short) (s - this.loudness);
                    }

                    //   System.out.println(s);
                    //=============== End of Process ============
                    // as the output stream is a byte array load the low/high integer
                    byte hexBase; // A byte of all ones
                    hexBase = (byte) 255;
                    byte b1 = (byte) (hexBase & s);
                    byte b2 = (byte) ((hexBase << (8) & s) >> 8);
                    out.write(b2);
                    out.write(b1);
                } // end of process this 200 bytes
            } // end of for loop = length of piece
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }
}

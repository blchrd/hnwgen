package io.hnwgen.hnw;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class HnwImage {
    int width, height;
    HnwRandom random;

    public HnwImage(int width, int height) {
        this.width = width;
        this.height = height;
        this.random = new HnwRandom();
    }

    public void generateTributeCover(String infile, String outfile, String title) throws IOException {
        BufferedImage sourceImg = ImageIO.read(new File(infile));
        BufferedImage img = new BufferedImage(sourceImg.getWidth(), sourceImg.getHeight(), BufferedImage.TYPE_3BYTE_BGR);

        Graphics2D graphic = img.createGraphics();
        graphic.drawImage(sourceImg, 0, 0, null);
        graphic.dispose();

        for (int i = 0; i < this.width; i++) {
            for (int j = 0; j < this.height; j++) {
                if (this.random.nextInt(4) == 2) {
                    int grey = this.random.nextInt(100);
                    int a = this.random.getRandomBetween(50, 150, false);

                    Color color = new Color(grey, grey, grey, a);

                    img.setRGB(i, j, color.getRGB());
                }
            }
        }

        addTitleOnImage(img, title);
        addBackgroundHexa(img);

        File file = new File(outfile);
        try {
            ImageIO.write(img, "jpg", file);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void generateImage(String outfile, String title, boolean taintImage) {
        JFrame frame = new JFrame();
        JPanel panel = new JPanel();

        BufferedImage img = new BufferedImage(this.width, this.height, BufferedImage.TYPE_3BYTE_BGR);
        frame.setSize(new Dimension(this.width, this.height));
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setContentPane(panel);
        for (int i = 0; i < this.width; i++) {
            for (int j = 0; j < this.height; j++) {
                int a = this.random.nextInt(50);
                int r = this.random.nextInt(100);
                int g = this.random.nextInt(100);
                int b = this.random.nextInt(100);

                Color color = new Color(r, g, b, a);

                img.setRGB(i, j, color.getRGB());
            }
        }

        if (taintImage) {
            img = tintingImage(img);
        }

        //background text
        addBackgroundHexa(img);

        //Add the title
        addTitleOnImage(img, title);

        File file = new File(outfile);
        try {
            ImageIO.write(img, "jpg", file);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    private void addBackgroundHexa(BufferedImage image) {
        String backgroundText;
        for (int j = 0; j < 100; j++) {
            for (int i = 0; i < 24; i++) {
                backgroundText = String.format("%8s", Integer.toHexString(this.random.nextInt(536862719)).replace(' ', '0'));
                Graphics backgroundImageText = image.getGraphics();
                backgroundImageText.setColor(new Color(100, 100, 100));
                backgroundImageText.setFont(new Font("Courier New", Font.PLAIN, 15));
                backgroundImageText.drawString(backgroundText, -4 + (i * 65), 12 + (j * 17));
            }
        }
    }

    private void addTitleOnImage(BufferedImage image, String title) {
        Graphics textTitle = image.getGraphics();
        textTitle.setColor(Color.GRAY);
        textTitle.setFont(new Font("Courier New", Font.BOLD, 55));
        textTitle.drawString(title, this.random.getRandomBetween(50, 550), this.random.getRandomBetween(150, 1100));
    }

    public BufferedImage tintingImage(BufferedImage sourceImage) {
        int WIDTH = sourceImage.getWidth();
        int HEIGHT = sourceImage.getHeight();
        BufferedImage taintedImage = new BufferedImage(WIDTH, HEIGHT, sourceImage.getType());

        int inputHue = this.random.nextInt(360);
        float hue = inputHue / 360.0f;

        for (int Y = 0; Y < HEIGHT; Y++) {
            for (int X = 0; X < WIDTH; X++) {
                int RGB = sourceImage.getRGB(X, Y);
                int R = (RGB >> 16) & 0xff;
                int G = (RGB >> 8) & 0xff;
                int B = (RGB) & 0xff;
                float[] HSV = new float[3];
                Color.RGBtoHSB(R, G, B, HSV);
                taintedImage.setRGB(X, Y, Color.getHSBColor(hue, HSV[1], HSV[2]).getRGB());
            }
        }

        return taintedImage;
    }
}

package io.hnwgen.hnw;

import io.hnwgen.utils.Constants;

import java.util.Objects;
import java.util.Properties;

public class HnwString {
    public enum Type{PROJECT_NAME, ALBUM_TITLE, COVER_TITLE, TRACK_TITLE}

    public static String getString(Type type, int seed, HnwRandom random, Properties config) {
        String string = "";
        String template = "";
        int randInt = 0;
        String randHex = "";

        switch (type) {
            case PROJECT_NAME:
                if (!config.containsKey(Constants.CONF_PROJECT_NAME)) {
                    return "unknown";
                }
                string = config.get(Constants.CONF_PROJECT_NAME).toString();
                break;
            case COVER_TITLE:
                if (!config.containsKey(Constants.CONF_COVER_ALBUM_TITLE_TEMPLATE)) {
                    template = "%%PROJECTNAME%% - %%ALBUMTITLE%%";
                } else {
                    template = config.get(Constants.CONF_COVER_ALBUM_TITLE_TEMPLATE).toString();
                }
                break;
            case ALBUM_TITLE:
                if (!config.containsKey(Constants.CONF_ALBUM_TITLE_TEMPLATE)) {
                    return "untitled";
                }
                template = config.get(Constants.CONF_ALBUM_TITLE_TEMPLATE).toString();
                randInt = seed;
                randHex = Integer.toHexString(randInt);
                if (Objects.equals(template, "")) {
                    string = "untitled";
                }
                break;
            case TRACK_TITLE:
                if (!config.containsKey(Constants.CONF_TRACK_TITLE_TEMPLATE)) {
                    return "untitled";
                }
                template = config.get(Constants.CONF_TRACK_TITLE_TEMPLATE).toString();
                randInt = random.nextInt(536862719) + seed;
                randHex = String.format("%8s", Integer.toHexString(randInt)).replace(' ', '0');
                if (Objects.equals(template, "")) {
                    string = "untitled";
                }
                break;
        }

        if (!Objects.equals(template, "")) {
            string = template;
            if (string.contains("%%PROJECTNAME%%")) {
                string = string.replace("%%PROJECTNAME%%", getString(Type.PROJECT_NAME, seed, random, config));
            }
            string = string.replace("%%RANDHEX%%", randHex);
            if (string.contains("%%ALBUMTITLE%%")) {
                string = string.replace("%%ALBUMTITLE%%", getString(Type.ALBUM_TITLE, seed, random, config));
            }
        }

        return string;
    }
}

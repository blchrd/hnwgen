package io.hnwgen;

import io.hnwgen.ffmpeg.FFMPEG;
import io.hnwgen.hnw.HnwImage;
import io.hnwgen.hnw.HnwRandom;
import io.hnwgen.hnw.HnwString;
import io.hnwgen.hnw.HnwTrack;
import io.hnwgen.utils.Utils;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

interface Callback {
    void changeStatus(String status);
}

public class HnwGenerator extends Thread {
    int length;
    int nbTracks;
    int nbLayers;
    boolean pitchChance;
    boolean variationChance;
    HnwRandom random;
    Callback callback;
    boolean taintCover;
    String tributePath;
    boolean withTributeCover;
    String tributeTitle;

    public HnwGenerator() {
        this.random = new HnwRandom();
    }

    public HnwGenerator(int length, int nbTracks, int nbLayers, boolean pitchChance, boolean variationChance, boolean taintCover, Callback callback) {
        this.length = length;
        this.nbTracks = nbTracks;
        this.nbLayers = nbLayers;
        this.callback = callback;
        this.pitchChance = pitchChance;
        this.variationChance = variationChance;
        this.taintCover = taintCover;
        this.tributePath = "";
        this.withTributeCover = false;
        this.tributeTitle = "";

        this.random = new HnwRandom();
    }

    @Override
    public void run() {
        try {
            this.generateHnwAlbum();
        } catch (InterruptedException | IOException e) {
            throw new RuntimeException(e);
        }

        this.callback.changeStatus("Generation done!");
    }

    /**
     * @param args the command line arguments
     */
    @SuppressWarnings("empty-statement")
    public static void main(String[] args) {
        HnwGenerator hnwGenerator = new HnwGenerator();

        //default value
        hnwGenerator.length = 20;
        hnwGenerator.nbTracks = 2;
        hnwGenerator.nbLayers = 3;
        hnwGenerator.pitchChance = false;
        hnwGenerator.taintCover = false;
        hnwGenerator.tributePath = "";
        hnwGenerator.withTributeCover = false;

        //parse the commandline argument
        for (String arg : args) {
            String[] parts = arg.split("=");
            switch (parts[0]) {
                case ("-h"), ("--help"):
                    String help = hnwGenerator.generateHelp();
                    System.out.print(help);
                    return;
                case ("--gui"):
                    HnwGui.main(args);
                    return;
                case ("--length"):
                    if (parts.length == 2) {
                        hnwGenerator.length = Integer.parseInt(parts[1]);
                    }
                    break;
                case ("--nbTracks"):
                    if (parts.length == 2) {
                        hnwGenerator.nbTracks = Integer.parseInt(parts[1]);
                    }
                    break;
                case ("--nbLayers"):
                    if (parts.length == 2) {
                        hnwGenerator.nbLayers = Integer.parseInt(parts[1]);
                    }
                    break;
                case ("--pitch"):
                    hnwGenerator.pitchChance = true;
                    break;
                case ("--variation"):
                    hnwGenerator.variationChance = true;
                    break;
                case ("--taintCover"):
                    hnwGenerator.taintCover = true;
                    break;
                case ("--onlycover"):
                    HnwImage coverImage = new HnwImage(1400, 1400);
                    coverImage.generateImage("generation/cover-test.jpg", "test_title", hnwGenerator.taintCover);
                    System.exit(0);
                    break;
                case ("--onlytributecover"):
                    if (parts.length == 2) {
                        HnwImage tributeCoverImage = new HnwImage(1400, 1400);
                        try {
                            tributeCoverImage.generateTributeCover(parts[1], "generation/tribute-cover-test.jpg", "Girls' Last Tour");
                            System.exit(0);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                    break;
                case ("--tribute"):
                    if (parts.length == 2) {
                        hnwGenerator.tributePath = parts[1];
                    }
                    break;
                case ("--tributetitle"):
                    if (parts.length == 2) {
                        hnwGenerator.tributeTitle = parts[1];
                    }
                    break;
                case ("--withcover"):
                    hnwGenerator.withTributeCover = true;
                    break;
            }
        }

        try {
            hnwGenerator.generateHnwAlbum();
        } catch (InterruptedException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void generateHnwAlbum() throws InterruptedException, IOException {
        Properties config = Utils.getConfiguration();
        if (!Objects.equals(this.tributePath, "")) {
            this.generateTributeHnwAlbum(this.tributePath);
            return;
        }

        //var declaration
        int loudness;
        int numTrack = 1;
        String dirName;
        String name;
        String infoText;
        java.io.File details;
        java.io.FileOutputStream log;

        if (this.nbLayers == 0) {
            this.nbLayers = 3;
        }

        //Title of Album
        int albumSeed = this.random.nextInt(2147450879);
        name = HnwString.getString(HnwString.Type.ALBUM_TITLE, albumSeed, random, config);

        //Create directory
        dirName = "generation/" + name;
        File dir = new File(dirName);
        int number = 1;
        while (dir.exists()) {
            dirName = "generation/" + name + "-" + number;
            dir = new File(dirName);
            number++;
        }
        if (!dir.mkdirs()) {
            System.out.println("Cannot create directory " + dirName);
            this.callback.changeStatus("Cannot create directory");
            return;
        }

        //Create working directory
        String workDirName = dirName + "/temp";
        File workDir = new File(workDirName);
        if (workDir.exists()) {
            Utils.deleteDirectory(workDirName);
        }
        if (!workDir.mkdirs()) {
            System.out.println("Cannot create directory " + workDirName);
            return;
        }

        //make the cover
        HnwImage coverImage = new HnwImage(1400, 1400);
        coverImage.generateImage(dirName + "/cover.jpg", HnwString.getString(HnwString.Type.COVER_TITLE, albumSeed, random, config), this.taintCover);

        loudness = this.random.nextInt(2500) + 600;

        //for debug
        System.out.println("Album : " + name);
        System.out.println("Number of track : " + this.nbTracks);
        System.out.println("Total length : " + this.length + " minute(s)");
        System.out.println("Loudness : " + loudness);
        //text details
        infoText = "Album : " + name + "\r\n";
        infoText += "Number of track : " + this.nbTracks + "\r\n";
        infoText += "Total length : " + this.length + " minute(s)\r\n";

        //3840 = 1m
        int trackLength;
        trackLength = 3840 * this.length;
        trackLength = trackLength / this.nbTracks;
        //make the tracks
        StringBuilder infoTextBuilder = new StringBuilder(infoText);
        for (int i = 1; i <= this.nbTracks; i++) {
            if (this.callback != null) {
                this.callback.changeStatus("Generation of track " + i + " out of " + this.nbTracks + "...");
            }

            //name of track
            name = numTrack + " - " + HnwString.getString(HnwString.Type.TRACK_TITLE, albumSeed, random, config);
            numTrack++;

            HnwTrack hnwTrack = this.generateWall(name, trackLength, loudness, workDirName, dirName + "/" + name + ".wav");

            infoTextBuilder.append("\r\n").append(hnwTrack.getTrackDetails());
        }
        infoText = infoTextBuilder.toString();

        //put details in a .txt file
        try {
            details = new java.io.File(dirName + "/details.txt");
            if (details.createNewFile()) {
                log = new java.io.FileOutputStream(details);
                log.write(infoText.getBytes());

                log.close();
            }
        } catch (Exception e) {
            System.out.println("ERROR !!!");
        }

        //Clean the directory
        Utils.deleteDirectory(workDirName);
    }

    public void generateTributeHnwAlbum(String path) throws IOException {
        File directoryPath = new File(path);
        //Create directories
        String workDirectory = path + "/work";
        File workDir = new File(workDirectory);
        if (workDir.exists()) {
            Utils.deleteDirectory(workDirectory);
        }

        if (!workDir.mkdirs()) {
            System.out.println("Cannot create directory " + workDirectory);
            return;
        }

        String finalDirectory = path + "/final";
        File dir = new File(finalDirectory);
        if (dir.exists()) {
            Utils.deleteDirectory(finalDirectory);
        }
        if (!dir.mkdirs()) {
            System.out.println("Cannot create directory " + finalDirectory);
            return;
        }
        FilenameFilter filenameFilter = (dir1, name) -> {
            String lowercaseName = name.toLowerCase();
            return lowercaseName.endsWith(".wav");
        };

        if (this.withTributeCover) {
            //generate tribute cover
            String coverPath = path + "/cover.jpg";
            File originalCover = new File(coverPath);
            if (originalCover.exists() && !Objects.equals(this.tributeTitle, "")) {
                HnwImage tributeCoverImage = new HnwImage(1400, 1400);
                tributeCoverImage.generateTributeCover(coverPath, finalDirectory + "/cover.jpg", this.tributeTitle);
            }
        }

        String[] contents = directoryPath.list(filenameFilter);
        for (int i = 0; i < Objects.requireNonNull(contents).length; i++) {
            System.out.println(contents[i]);
            String originalAudio = path + "/" + contents[i];
            String wallAudio = workDirectory + "/" + contents[i] + "-wall-" + i + ".wav";
            //generate wall
            this.generateWall("wall-temp-" + i, 3840 * 10, this.random.nextInt(2500) + 600, workDirectory, wallAudio);

            //mix original audio and wall
            String trackPath = this.generateHnwTributeTrack(originalAudio, wallAudio, workDirectory, i);

            Utils.copyFile(trackPath, finalDirectory + "/" + contents[i]);
        }

        //Clean the directory
        Utils.deleteDirectory(workDirectory);
    }

    public String generateHnwTributeTrack(String originalAudioPath, String wallPath, String workDirectory, int index) {
        return generateHnwTributeTrack(originalAudioPath, wallPath, workDirectory, index, 60, 600);
    }

    public String generateHnwTributeTrack(String originalAudioPath, String wallPath, String workDirectory, int index, int originalAudioDurationInSecond, int trackDurationInSecond) {
        int time = Math.max(originalAudioDurationInSecond, 20);
        int trackDuration = Math.max(trackDurationInSecond, 300);

        //made silent audio to mix with original audio file
        String silentAudio = workDirectory + "/silence.wav";
        FFMPEG.generateSilentTrack(silentAudio, trackDuration);

        //Path to the temporary audio file
        String cutAudio = workDirectory + "/cut-" + index + ".wav";
        String cutAudioFadeOut = workDirectory + "/cut-" + index + "-fo.wav";
        String cutAudioFinal = workDirectory + "/cut-" + index + "-fi.wav";
        String wallAudioFadeIn = workDirectory + "/wall-" + index + "-fi.wav";
        String wallAudioFinalVolume = workDirectory + "/wall-" + index + "-fv.wav";
        String mixedAudio = workDirectory + "/mixes-" + index + ".wav";
        String finalAudioBeforeFadeout = workDirectory + "/final-bfo-" + index + ".wav";
        String finalAudio = workDirectory + "/final-" + index + ".wav";

        //cut the end of the original audio + fade out
        FFMPEG.cut(originalAudioPath, cutAudio, 0, time);
        FFMPEG.fadeOut(cutAudio, cutAudioFadeOut, (time / 3), ((time / 3) * 2));
        //mix silent and cut audio
        FFMPEG.mixLayers(new String[]{silentAudio, cutAudioFadeOut}, cutAudioFinal);
        //fade in the wall and turn the volume to 0 for the first seconds
        FFMPEG.fadeIn(wallPath, wallAudioFadeIn, time, 10);
        FFMPEG.cutVolume(wallAudioFadeIn, wallAudioFinalVolume, 0, 10);
        //Mix wall and cutted original audio + fade out the final result
        FFMPEG.mixLayers(new String[]{cutAudioFinal, wallAudioFinalVolume}, mixedAudio);
        FFMPEG.finalize(mixedAudio, finalAudioBeforeFadeout);
        FFMPEG.fadeOut(finalAudioBeforeFadeout, finalAudio, trackDuration / 20, trackDuration - 30);

        return finalAudio;
    }

    public HnwTrack generateWall(String title, int trackLength, int loudness, String workDirectory, String finalFilePath, int volume) {
        HnwTrack hnwTrack = new HnwTrack(title, trackLength, workDirectory, loudness, this.nbLayers, this.pitchChance, this.variationChance, random);
        hnwTrack.generateLayers();
        hnwTrack.mixLayers();

        if (this.nbLayers == 1) {
            //If there is only one layer, just copy the file in the final directory
            try {
                Utils.copyFile(hnwTrack.getFilePath(), finalFilePath);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            //The layering lower the volume of the track, therefore we need to adapt the final volume
            FFMPEG.finalize(hnwTrack.getFilePath(), finalFilePath, volume);
        }

        return hnwTrack;
    }

    public HnwTrack generateWall(String title, int trackLength, int loudness, String workDirectory, String finalFilePath) {
        return generateWall(title, trackLength, loudness, workDirectory, finalFilePath, 3);
    }

    public String generateHelp() {
        return """
                Usage:\s
                java -jar hnwgen.jar [options]

                Options:\s
                \t-h --help\t\t\tDisplay this help
                \t--gui\t\t\t\tDisplay the gui
                \t--length=<m>\t\t\tLength of album in minute [default: 20]
                \t--nbTrack=<nb>\t\t\tNumber of tracks in album [default: 2]
                \t--nbLayers=<nb>\t\t\tNumber of layers for each track [default: 3]
                \t--pitch\t\t\t\tEnable pitch change in layer generation
                \t--variation\t\t\tEnable wall variation in layer generation
                \t--taintCover\t\t\tEnable the tainting of generated cover
                \t--onlycover\t\t\tOnly generate the cover
                \t--onlytributecover=<file>\tOnly generate the tribute cover, base on <file>
                \t--tribute=<dir>\t\t\tGenerate a tribute album with all wav file in <dir>, the track will have 10 minutes length
                \t--withcover\t\t\tEnable the generation of cover for tribute album. Need a file named "cover.jpg" in tribute directory
                """;
    }
}

package io.hnwgen;

import io.hnwgen.utils.Utils;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Properties;

import static io.hnwgen.utils.Constants.*;

public class HnwGui implements Callback {
    private JFrame mainFrame;
    private JPanel mainPanel;
    private JLabel headerLabel;
    private JLabel statusLabel;
    private JPanel controlPanel;

    // https://www.geeksforgeeks.org/java-swing-jmenubar/

    public HnwGui() {
        mainWindow();
    }

    public static void main(String[] args) {
        HnwGui hnwGui = new HnwGui();
        hnwGui.hnwMainControlPanel();
    }

    private void mainWindow() {
        mainFrame = new JFrame("HNW Generator");
        mainFrame.setSize(550, 550);
        URL iconURL = getClass().getResource("/io/hnwgen/hnwgen-logo.png");
        if (iconURL != null) {
            ImageIcon icon = new ImageIcon(iconURL);
            mainFrame.setIconImage(icon.getImage());
        }
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        // Create menu
        JMenuBar mainMenuBar = getMainMenuBar();
        mainFrame.setJMenuBar(mainMenuBar);

        mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(5, 1));
        mainPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

        headerLabel = new JLabel("", JLabel.LEFT);
        statusLabel = new JLabel("", JLabel.CENTER);
        statusLabel.setSize(540, 70);

        controlPanel = new JPanel();
        controlPanel.setLayout(new GridLayout(3, 2));

        mainPanel.add(headerLabel);
        mainPanel.add(controlPanel);
        mainPanel.add(statusLabel);

        mainFrame.add(mainPanel);
        mainFrame.setVisible(true);
    }

    private JMenuBar getMainMenuBar() {
        JMenuBar mainMenuBar = new JMenuBar();
        JMenu mainMenu = new JMenu("Menu");
        JMenuItem configMenu = new JMenuItem("Config");
        configMenu.addActionListener(e -> {
            JFrame configFrame = configurationWindow();
            configFrame.setVisible(true);
            mainFrame.setEnabled(false);
        });
        JMenuItem quitMenu = new JMenuItem("Quit");
        quitMenu.addActionListener(e -> System.exit(0));
        mainMenu.add(configMenu);
        mainMenu.add(quitMenu);
        mainMenuBar.add(mainMenu);
        return mainMenuBar;
    }

    private void hnwMainControlPanel() {
        headerLabel.setText("""
                <html>HNW Generation<br/><br/>
                Only number must be entered below, it will not work in other case.<br/>
                Leave empty for 20 minutes album with 2 tracks, 3 layers, no pitch, no variation</html>
                """);
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.getDefault());
        DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
        decimalFormat.setGroupingUsed(false);

        JLabel lengthInputLabel = new JLabel("Length (In minutes)");
        JTextField lengthInput = new JTextField();
        JLabel nbTrackInputLabel = new JLabel("Number of track");
        JTextField nbTrackInput = new JTextField();
        JLabel nbLayerInputLabel = new JLabel("Number of layer");
        JTextField nbLayerInput = new JTextField();
        JLabel pitchChanceCheckBoxLabel = new JLabel("Pitch");
        JCheckBox pitchChanceCheckBox = new JCheckBox();
        JLabel variationChanceCheckBoxLabel = new JLabel("Variation");
        JCheckBox variationChanceCheckBox = new JCheckBox();
        JLabel taintCoverCheckBoxLabel = new JLabel("Taint album cover");
        JCheckBox taintCoverCheckBox = new JCheckBox();

        JButton generateButton = new JButton("BUILD HNW");
        generateButton.addActionListener(e -> {
            statusLabel.setText("Generation ongoing...");
            HnwGenerator hnwGenerator = new HnwGenerator(
                    (!lengthInput.getText().isEmpty()) ? Integer.parseInt(lengthInput.getText()) : 20,
                    (!nbTrackInput.getText().isEmpty()) ? Integer.parseInt(nbTrackInput.getText()) : 2,
                    (!nbLayerInput.getText().isEmpty()) ? Integer.parseInt(nbLayerInput.getText()) : 3,
                    (pitchChanceCheckBox.isSelected()),
                    (variationChanceCheckBox.isSelected()),
                    (taintCoverCheckBox.isSelected()),
                    this
            );
            hnwGenerator.start();

            mainFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
            mainFrame.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent windowEvent) {
                    if (hnwGenerator.isAlive()) {
                        int reply = JOptionPane.showConfirmDialog(null, "There is a generation ongoing. Do you really want to close ?");
                        if (reply == JOptionPane.NO_OPTION || reply == JOptionPane.CANCEL_OPTION) {
                            return;
                        }
                    }
                    System.exit(0);
                }
            });

            mainFrame.setVisible(true);
        });

        controlPanel.add(lengthInputLabel);
        controlPanel.add(lengthInput);
        controlPanel.add(nbTrackInputLabel);
        controlPanel.add(nbTrackInput);
        controlPanel.add(nbLayerInputLabel);
        controlPanel.add(nbLayerInput);

        JPanel checkBoxPanel = new JPanel();
        checkBoxPanel.add(pitchChanceCheckBox);
        checkBoxPanel.add(pitchChanceCheckBoxLabel);
        checkBoxPanel.add(variationChanceCheckBox);
        checkBoxPanel.add(variationChanceCheckBoxLabel);
        checkBoxPanel.add(taintCoverCheckBox);
        checkBoxPanel.add(taintCoverCheckBoxLabel);

        mainPanel.add(checkBoxPanel);
        mainPanel.add(statusLabel);
        mainPanel.add(generateButton);
        mainFrame.setVisible(true);
    }

    private JFrame configurationWindow() {
        Properties config = Utils.getConfiguration();

        JFrame configFrame = new JFrame("Configuration");
        configFrame.setSize(450, 400);
        URL iconURL = getClass().getResource("/io/hnwgen/hnwgen-logo.png");
        if (iconURL != null) {
            ImageIcon icon = new ImageIcon(iconURL);
            configFrame.setIconImage(icon.getImage());
        }
        configFrame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        configFrame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent) {
                configFrame.setVisible(false);
                mainFrame.setEnabled(true);
                mainFrame.requestFocus();
            }
        });

        JPanel configPanel = new JPanel();

        GridLayout layout = new GridLayout(5, 1);
        layout.setVgap(5);
        layout.setHgap(5);
        configPanel.setLayout(layout);
        configPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

        JLabel descriptionLabel = new JLabel("", JLabel.LEFT);
        descriptionLabel.setText("""
                <html><p>For template field, you can use the following placeholder :</p>
                <ul>
                <li>%%PROJECTNAME%%: Name of the project</li>
                <li>%%ALBUMTITLE%%: Title of the album</li>
                <li>%%RANDHEX%%: Random hexadecimal number</li>
                </ul>
                </html>
                """);

        JLabel projectNameLabel = new JLabel("Project name");
        JTextField projectNameInput = new JTextField(config.getProperty(CONF_PROJECT_NAME));
        JLabel albumTitleTemplateLabel = new JLabel("Album title template");
        JTextField albumTitleTemplateInput = new JTextField(config.getProperty(CONF_ALBUM_TITLE_TEMPLATE));
        JLabel coverAlbumTitleTemplateLabel = new JLabel("Cover album title template");
        JTextField coverAlbumTitleTemplateInput = new JTextField(config.getProperty(CONF_COVER_ALBUM_TITLE_TEMPLATE));
        JLabel trackTitleTemplateLabel = new JLabel("Track title template");
        JTextField trackTitleTemplateInput = new JTextField(config.getProperty(CONF_TRACK_TITLE_TEMPLATE));

        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(event -> {
            configFrame.setVisible(false);
            mainFrame.setEnabled(true);
            mainFrame.requestFocus();
        });

        JButton saveButton = new JButton("Save");
        saveButton.addActionListener(event -> {
            config.setProperty(CONF_PROJECT_NAME, projectNameInput.getText());
            config.setProperty(CONF_ALBUM_TITLE_TEMPLATE, albumTitleTemplateInput.getText());
            config.setProperty(CONF_COVER_ALBUM_TITLE_TEMPLATE, coverAlbumTitleTemplateInput.getText());
            config.setProperty(CONF_TRACK_TITLE_TEMPLATE, trackTitleTemplateInput.getText());

            Utils.setConfiguration(config);
            mainFrame.setEnabled(true);
            mainFrame.requestFocus();
        });

        configPanel.add(projectNameLabel);
        configPanel.add(projectNameInput);
        configPanel.add(albumTitleTemplateLabel);
        configPanel.add(albumTitleTemplateInput);
        configPanel.add(coverAlbumTitleTemplateLabel);
        configPanel.add(coverAlbumTitleTemplateInput);
        configPanel.add(trackTitleTemplateLabel);
        configPanel.add(trackTitleTemplateInput);

        configPanel.add(cancelButton);
        configPanel.add(saveButton);

        JPanel mainConfigPanel = new JPanel();
        mainConfigPanel.setLayout(new GridLayout(2, 1));
        mainConfigPanel.setBorder(new EmptyBorder(10, 10, 10, 10));

        mainConfigPanel.add(descriptionLabel);
        mainConfigPanel.add(configPanel);

        configFrame.add(mainConfigPanel);

        return configFrame;
    }

    @Override
    public void changeStatus(String status) {
        statusLabel.setText(status);
    }
}

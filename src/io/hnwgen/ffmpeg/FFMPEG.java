package io.hnwgen.ffmpeg;

import io.hnwgen.utils.Utils;

public class FFMPEG {
    public static void convertFile(String in, String out) {
        Utils.executeCmd("ffmpeg -y -i \"" + in + "\" \"" + out + "\"");
    }

    public static void applyEq(String in, String out, int frequency, int width, int gain) {
        Utils.executeCmd("ffmpeg -y -i \"" + in + "\" -af \"equalizer=f=" + frequency + ":width_type=h:width=" + width + ":g=" + gain + "\" \"" + out + "\"");
    }

    public static void applyPitch(String in, String out, double rate) {
        Utils.executeCmd("ffmpeg -y -i \"" + in + "\" -af \"asetrate=44100*" + rate + ",aresample=44100,atempo=1/" + rate + "\" \"" + out + "\"");
    }

    public static void cutVolume(String in, String out, int startTime, int endTime) {
        Utils.executeCmd("ffmpeg -y -i \"" + in + "\" -af \"volume=enable='between(t," + startTime + "," + endTime + ")':volume=0\" \"" + out + "\"");
    }

    public static void mixLayers(String[] layers, String out) {
        String cmdLineLayering = "";

        if (layers.length > 1) {
            for (String layer : layers) {
                cmdLineLayering += "-i \"" + layer + "\" ";
            }

            Utils.executeCmd("ffmpeg " + cmdLineLayering + "-filter_complex amix=inputs=" + layers.length + ":duration=longest \"" + out + "\"");
        } else {
            //if we have only one layer, just rename the file
            Utils.renameFile(layers[0], out);
        }
    }

    public static void generateSilentTrack(String out, int durationInSeconds) {
        Utils.executeCmd("ffmpeg -f lavfi -i \"anullsrc=r=22050:cl=mono\" -t \"" + durationInSeconds + "\" -ar 44100 -y \"" + out + "\"");
    }

    public static void resample(String in, String out) {
        Utils.executeCmd("ffmpeg -y -i \"" + in + "\" -ar 44100 \"" + out + "\"");
    }

    public static void cut(String in, String out, int from, int time) {
        Utils.executeCmd("ffmpeg -y -ss " + from + " -i \"" + in + "\" -t " + time + " \"" + out + "\"");
    }

    public static void fadeIn(String in, String out, int timeInSecond, int startTime) {
        Utils.executeCmd("ffmpeg -i \"" + in + "\" -af \"afade=t=in:st=" + startTime + ":d=" + timeInSecond + "\" \"" + out + "\"");
    }

    public static void fadeOut(String in, String out, int timeInSecond, int startTime) {
        Utils.executeCmd("ffmpeg -i \"" + in + "\" -af \"afade=t=out:st=" + startTime + ":d=" + timeInSecond + "\" \"" + out + "\"");
    }

    public static void fadeOut(String in, String out, int timeInSecond) {
        fadeOut(in, out, timeInSecond, -1);
    }

    public static void finalize(String in, String out) {
        finalize(in, out, 3);
    }

    public static void finalize(String in, String out, int volume) {
        Utils.executeCmd("ffmpeg -y -i \"" + in + "\" -filter:a \"volume=" + volume + "\" \"" + out + "\"");
    }
}

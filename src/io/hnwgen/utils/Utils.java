package io.hnwgen.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Properties;

import static io.hnwgen.utils.Constants.*;

public class Utils {
    private static OS os = null;

    public static OS getOS() {
        if (os == null) {
            String operSys = System.getProperty("os.name").toLowerCase();
            if (operSys.contains("win")) {
                os = OS.WINDOWS;
            } else if (operSys.contains("nix") || operSys.contains("nux")
                    || operSys.contains("aix")) {
                os = OS.LINUX;
            } else if (operSys.contains("mac")) {
                os = OS.MAC;
            } else if (operSys.contains("sunos")) {
                os = OS.SOLARIS;
            }
        }
        return os;
    }

    public static void executeCmd(String cmdLine) {
        OS os = getOS();
        System.out.println(cmdLine);

        try {
            ProcessBuilder builder = new ProcessBuilder();
            if (os == OS.WINDOWS) {
                builder.command("cmd.exe", "/C", cmdLine);
            } else if (os == OS.LINUX) {
                builder.command("/bin/sh", "-c", cmdLine);
            }
            builder.directory(new File(System.getProperty("user.dir")));
            Process process = builder.start();
            process.waitFor();
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean renameFile(String oldFilePath, String newFilePath) {
        File oldFile = new File(oldFilePath);
        File newFile = new File(newFilePath);

        return oldFile.renameTo(newFile);
    }

    public static void copyFile(String sourceFilePath, String destinationFilePath) throws IOException {
        File src = new File(sourceFilePath);
        File dest = new File(destinationFilePath);

        Files.copy(src.toPath(), dest.toPath());
    }

    public static void deleteDirectory(String path) throws IOException {
        Path directory = Paths.get(path);

        if (Files.exists(directory)) {
            Files.walkFileTree(directory, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path path, BasicFileAttributes basicFileAttributes) throws IOException {
                    Files.delete(path);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path directory, IOException ioException) throws IOException {
                    Files.delete(directory);
                    return FileVisitResult.CONTINUE;
                }
            });
        }
    }

    public static Properties getConfiguration() {
        Properties properties = new Properties();

        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream("hnw.conf");
            properties.load(fileInputStream);
            fileInputStream.close();
        } catch (Exception e) {
            //If exception, we print it in the console then we put default value
            System.out.println("Error with configuration : hnw.conf.example not found");
            System.out.println("Using default configuration");
            properties.put(CONF_PROJECT_NAME, "unknown");
            properties.put(CONF_ALBUM_TITLE_TEMPLATE, "untitled");
            properties.put(CONF_COVER_ALBUM_TITLE_TEMPLATE, "%%PROJECTNAME%% - %%ALBUMTITLE%%");
            properties.put(CONF_TRACK_TITLE_TEMPLATE, "untitled");
        }

        return properties;
    }

    public static void setConfiguration(Properties properties) {
        try {
            File myObj = new File("hnw.conf");
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }

            FileOutputStream fout = new FileOutputStream("hnw.conf", false);

            properties.store(fout, "");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

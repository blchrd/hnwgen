package io.hnwgen.utils;

public class Constants {
    // Operating systems
    public enum OS {
        WINDOWS, LINUX, MAC, SOLARIS
    }

    // Configuration value
    public static String CONF_PROJECT_NAME="project_name";
    public static String CONF_ALBUM_TITLE_TEMPLATE="album_title_template";
    public static String CONF_COVER_ALBUM_TITLE_TEMPLATE="cover_album_title_template";
    public static String CONF_TRACK_TITLE_TEMPLATE="track_title_template";
}

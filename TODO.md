* Make the directory output configurable in `hnw.conf`
* Add stereo mixing if there is multiple layer (by example : if 2 layers,
one 40% to the left, other 40% to the right)
* GUI
  * add some tweaking for the EQ
  * search how to add progress bar for the generation of the track
* Add functionnality to select noise input (instead of generate one)
* Try implement SoX http://sox.sourceforge.net/ in addition of FFMPEG effect with a code like the one below (it's Python code)

```python
import os

effects = [
  'overdrive 10',
  'bass 15',
  'pitch -500',
  'overdrive 15',
  'reverb',
  'pitch -1000',
  'overdrive 10',
  'bass 10',
  'reverb',
  'bass 20',
  'overdrive 5',
  'highpass 20',
  'reverb',
]

sox_command = 'sox "' + source_file + '" "' + target_file + '" ' + effect_args
os.system(sox_command)
```